#!/bin/bash

set -e

select_multiarchname()
{
    echo "Please select the desired architecture:"
    echo "1): i386-linux-gnu"
    echo "2): x86_64-linux-gnu"
    echo "3): armeabi-linux-android"
    echo "4): armeabi_v7a-linux-android"
    read SELECTEDOPTION
    if [ x"$SELECTEDOPTION" = x"1" ]; then
        MULTIARCHNAME=i386-linux-gnu
    elif [ x"$SELECTEDOPTION" = x"2" ]; then
        MULTIARCHNAME=x86_64-linux-gnu
    elif [ x"$SELECTEDOPTION" = x"3" ]; then
        MULTIARCHNAME=armeabi-linux-android
    elif [ x"$SELECTEDOPTION" = x"4" ]; then
        MULTIARCHNAME=armeabi_v7a-linux-android
    else
        echo "Invalid option selected!"
        select_multiarchname
    fi
}

linux_build()
{
    if [ x"$BUILDTYPE" = xdebug ]; then
        CFLAGS="-ggdb"
        CXXFLAGS="-ggdb"
    fi

    if [ -z "$OPTIMIZATION" ]; then
        CFLAGS="$CFLAGS -O2"
        CXXFLAGS="$CXXFLAGS -O2"
    else
        CFLAGS="$CFLAGS -O$OPTIMIZATION"
        CXXFLAGS="$CXXFLAGS -O$OPTIMIZATION"
    fi

    mkdir "$BUILDDIR"
    rm -rf "$PREFIXDIR"

    ( JPEGDIR="${PWD}/../library-jpeg-turbo/${MULTIARCHNAME}"
      PNGDIR="${PWD}/../library-png16/${MULTIARCHNAME}"
      TIFFDIR="${PWD}/../library-tiff/${MULTIARCHNAME}"
      export CFLAGS="-I$PNGDIR/include/ -I$JPEGDIR/include/ -I$TIFFDIR/include/"
      export LDFLAGS="-L$PNGDIR/lib/ -L$JPEGDIR/lib/ -L$TIFFDIR/lib/"
      CFLAGS="$CFLAGS -Dpng_set_gray_1_2_4_to_8=png_set_expand_gray_1_2_4_to_8"
      cd "$BUILDDIR"
      ../source/configure \
        --prefix="$PREFIXDIR" \
        --disable-static \
        --with-examples=no \
        --enable-game-formats=no \
        --enable-ILU=yes \
        --enable-png \
        --enable-tga \
        --enable-tiff \
        --enable-jpeg
      make -j $(nproc)
      make install
      rm -rf "$PREFIXDIR"/{bin,lib/*.la,lib/pkgconfig,share}
      chrpath -d "$PREFIXDIR"/lib/libILU.so.1.1.0 )
      
    # clean up afterwards
    ( cd source
      git clean -df .
      git checkout . )
}

if [ -z "$MULTIARCHNAME" ]; then
    echo "\$MULTIARCHNAME is not set!"
    select_multiarchname
fi

if [ -z "$BUILDDIR" ]; then
    BUILDDIR="build_$MULTIARCHNAME"
fi

if [ -z "$PREFIXDIR" ]; then
    PREFIXDIR="$PWD/$MULTIARCHNAME"
fi

case "$MULTIARCHNAME" in
i386-linux-gnu)
    linux_build;;
x86_64-linux-gnu)
    linux_build;;
*)
    echo "$MULTIARCHNAME is not (yet) supported by this script."
    exit 1;;
esac

install -m664 source/COPYING "$PREFIXDIR"/lib/LICENSE-devil.txt

rm -rf "$BUILDDIR"

echo "DevIL for $MULTIARCHNAME is ready."
