Website
=======
http://openil.sourceforge.net/

License
=======
custom license (see the file source/COPYING)

Version
=======
1.7.8

Source
======
DevIL-1.7.8.tar.gz (sha256: 682ffa3fc894686156337b8ce473c954bf3f4fb0f3ecac159c73db632d28a8fd)

Requires
========
* jpeg-turbo
* png16
* tiff